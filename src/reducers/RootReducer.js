import {combineReducers} from "redux";
import CategoryListReducer from "./CategotyListReducer";
import CategoryMultipleReducer from "./CategoryMultipleReducer";

const RootReducer = combineReducers({
  CategoryList: CategoryListReducer,
  Category: CategoryMultipleReducer
});

export default RootReducer;