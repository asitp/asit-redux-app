const DefaultState = {
  loading: false,
  data: [],
  errorMsg: "",
  count: 0
};

const   CategoryListReducer = (state = DefaultState, action) => {
  switch (action.type) {
    case "CAT_LIST_LOADING":
      return {
        ...state,
        loading: true,
        errorMsg: ""
      };
    case "CAT_LIST_FAIL":
      return {
        ...state,
        loading: false,
        errorMsg: "unable to get categoty"
      };
    case "CAT_LIST_SUCCESS":
      return {
        ...state,
        loading: false,
        data: action.payload,
        errorMsg: "",
        count: action.payload.length
      };
    default:
      return state
  }
};

export default CategoryListReducer