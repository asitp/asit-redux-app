const DefaultState = {
  loading: false,
  data: [],
  errorMsg: ""
};

const CategoryMultipleReducer = (state = DefaultState, action) => {
  switch (action.type) {
    case "CAT_MULTIPLE_LOADING":
      return {
        ...state,
        loading: true,
        errorMsg: ""
      };
    case "CAT_MULTIPLE_FAIL":
      return {
        ...state,
        loading: false,
        errorMsg: "unable to find category"
      };
    case "CAT_MULTIPLE_SUCCESS":
      return {
        ...state,
        loading: false,
        errorMsg: "",
        data: [...state.data,...action.payload],
      };
    default:
      return state
  }
};

export default CategoryMultipleReducer