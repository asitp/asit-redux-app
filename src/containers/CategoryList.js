import React, {useEffect} from "react";
import {useDispatch, useSelector} from "react-redux";
import _ from "lodash";
import {GetCatCategories} from "../actions/categoryActions";
import {Link} from "react-router-dom";

const CategoryList = (props) => {
  const dispatch = useDispatch();
  const categoryList = useSelector(state => state.CategoryList);

  useEffect(() => {
    FetchData(1)
  }, []);

  const FetchData = (page = 1) => {
    dispatch(GetCatCategories(page))
  }

  const ShowData = () => {
    if (categoryList.loading) {
      return <p>Loading...</p>
    }

    if (!_.isEmpty(categoryList.data)) {
      return(
        <div className={"list-wrapper"}>
          <h1>All Catagory</h1>
          {categoryList&&categoryList.data.map((each, i) => {
            return(
              <div className={"category-item"} key={i} >
                <p>{each.name}</p>
                <Link to={`/${each.id}`}>View</Link>
              </div>
            )
          })}
        </div>
      )
    }

    if (categoryList.errorMsg !== "") {
      return <p>{categoryList.errorMsg}</p>
    }

    return <p>unable to get data</p>
  };

  return(
    <div>
      {ShowData()}
    </div>
  )
};

export default CategoryList