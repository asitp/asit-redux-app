import React from "react";
import {useDispatch, useSelector} from "react-redux";
import {GetCatDetails} from "../actions/categoryActions";
import _ from "lodash";

const Category = (props) => {
  const categoryId = props.match.params.category;
  const dispatch = useDispatch();
  const categotyState = useSelector(state => state.Category);

  React.useEffect(() => {
    dispatch(GetCatDetails(0,categoryId))
  }, []);

  const ShowData = () => {
    if (!_.isEmpty(categotyState.data)) {
      const catData = categotyState.data;
      //console.log("???????/",catData)
      return(
        <div>
          <div className={"item"}>
        
            {
              catData&&catData.map((each, i) =>
                <img src={each.url} height={400} width={500} key={i}  alt=""/>
              )
            }
          </div>

         <div className={"load-button"}>
               <button className={"button button1"}
                 onClick={ () =>  dispatch(GetCatDetails(catData.length,categoryId))}>
                 Load more
               </button>
          </div>
          
        </div>
      )
    }

    if (categotyState.loading) {
      return <p>Loading...</p>
    }

    if (categotyState.errorMsg !== "") {
      return <p>{categotyState.errorMsg}</p>
    }

    return <p>error getting categoty</p>
  }

  return(
    <div className={"cat"}>
      <h1>Catagory Details</h1>
      <h1>{categoryId}</h1>
      {ShowData()}
    </div>
  )
};

export default Category