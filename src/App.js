import React from 'react';
import './App.css';
import {Switch, Route, Redirect} from "react-router-dom";
import CategoryList from "./containers/CategoryList";
import Category from "./containers/Category";

function App() {
  return (
    <div className="App">
      <Switch>
        <Route path={"/"} exact component={CategoryList} />
        <Route path={"/:category"} exact component={Category} />
        <Redirect to={"/"} />
      </Switch>
    </div>
  );
}

export default App;
