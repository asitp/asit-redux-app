import axios from "axios";

export const GetCatCategories = () => async dispatch => {
  try {
    dispatch({
      type: "CAT_LIST_LOADING"
    });

    const res = await axios.get(`https://api.thecatapi.com/v1/categories`)
    dispatch({
      type: "CAT_LIST_SUCCESS",
      payload: res.data
    })
  } catch (e) {
    dispatch({
      type: "CAT_LIST_FAIL",
    })
  }
};

export const GetCatDetails = (skip=0,category=1) => async dispatch => {
  try {
    dispatch({
      type: "CAT_MULTIPLE_LOADING"
    });

    const res = await axios.get(`https://api.thecatapi.com/v1/images/search?limit=10&skip=${skip}&category_ids=${category}`);
    dispatch({
      type: "CAT_MULTIPLE_SUCCESS",
      payload: res.data,
    })
  } catch (e) {
    dispatch({
      type: "CAT_MULTIPLE_FAIL",
    })
  }
};